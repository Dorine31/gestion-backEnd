const express = require('express')
// const knex = require('knex')
// const bcrypt = require('bcrypt');
// const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser')
const cors = require('cors')

// let config = require('./knexfile.js')
// let database = knex(config.development)

const app = express()

// req.body parse and encoded
const bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(cors({credentials: true, origin: 'http://localhost:8080'}))

// middlewares
const middlewareRegister = require('./middlewares/register');
const middlewareLogin = require('./middlewares/login');
const middlewareAuthorisation = require('./middlewares/authorisation');
const middlewareAccount = require('./middlewares/account');
const middlewareGetAccount = require('./middlewares/getAccount');
const middlewareUpdateProfile = require('./middlewares/updateProfile');
const middlewareGetIdAccount = require('./middlewares/getIdAccount')
const middlewareAddMvmt = require('./middlewares/addMvmt')
const middlewareGetAllMovmts = require('./middlewares/getAllMovments')
// ROUTES
app.route('/')
   .get((req, res) => {
       res.send('accueil')
   })

app.route('/register')
   .get((req, res) => {
       res.send('register')
   })
   .post(middlewareRegister)

app.route('/login')
   .get((req, res) => {
       res.send('login')
   })
   .post(middlewareLogin)

app.use(cookieParser())

// log in
app.use(middlewareAuthorisation)

app.route('/user/:id/account')
   .get(middlewareGetAccount)
   .post(middlewareAccount)

app.route('/user/:id/getIdAccount')
   .put(middlewareGetIdAccount)

app.route('/user/:id/account/:idAccount/addMvmt')
   .post(middlewareAddMvmt)

app.route('/user/:id/account/:idAccount/getMvmts')
   .get(middlewareGetAllMovmts)

app.route('/user/:id/upload')
   .post(middlewareUpdateProfile)


let port = process.env.PORT || 3001;

let server = app.listen(port, function() {
        console.log('Express server listening on port ' + port)
 });