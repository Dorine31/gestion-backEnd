const knex = require('knex')
let config = require('../knexfile.js')
let database = knex(config.development)
let jwt = require('jsonwebtoken')

module.exports = async (req, res, next) => {
  let reqPath = req.path
  // console.log('1', req.originalUrl) // A utiliser pour connaitre le vrai lien !
  if (reqPath.startsWith('/account') || reqPath.startsWith('/user')) {
      console.log('req autorisation', req.cookies)
      let cookie = req.cookies.cookie

      let decoded = jwt.verify(cookie, 'secret')
      let id = decoded["id"]
      
      // Verify id exists in database
      let selectUser = await database.select('*').from('User').where({id: id})
 
      if(selectUser.length > 0) {
        next()
      }
      else {
        res.status(403).json("Accès refusé")
      }
  } else {
    next()
  }
}
