const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    // Fetch the data
    console.log('req updateprofil', req.body)
    const {name, email, path} = req.body
    const id = req.params.id
    let updateUser = async () => {
        try {
            // Insert data into database
            let update = await database.into('User').update({fullname: name, email: email, avatar_path: path}).where({id: id})
            res.end('Profil mis à jour.')
        } catch {
            error => res.send(error)
        }
    }
    updateUser()
};