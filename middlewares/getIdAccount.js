const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    // Fetch the data
    const accountNumber = req.body.accNumber
    const id = req.params.id
    // Fetch idAccount into database
    try {
        let getIdAccount = await database.select('*').from('Account').where({acc_number: accountNumber, user_id: id});
        if (getIdAccount.length > 0) {
            const balance = getIdAccount[0].balance
            const idAccount = getIdAccount[0].id
            res.status(200).send({idAccount: idAccount, balance: balance})
            next()
        } else {
            res.send('erreur de numéro de compte !')
        }
    } catch(err) {
        res.status(401).send(err)
    }
};