const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    // Fetch the data
    const id = req.params.id
    try {
        // Insert data into database
        let getAccount = await database.select('*').from('Account').where({id: id})
        if (getAccount.length > 0) {
            res.send(getAccount)
        } else {
            res.end()
        }
    } catch {
        error => res.send(error)
    }
};