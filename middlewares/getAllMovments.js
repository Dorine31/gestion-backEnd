const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    const reqPath = req.path;
    // Fetch the data
    const id = req.params.id
    const idAccount = req.params.idAccount
    try {
        // Insert data into database
        let getAllAccounts = await database.select('*').from('Movement').where({id: id, acc_id: idAccount})
        if (getAllAccounts.length > 0) {
            res.send(getAllAccounts)
        } else {
            res.send("getAllAccounts is empty")
        }
    } catch {
        error => res.send("erreur dans le try", error)
    }
};