const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    // Fetch the data
    console.log('req account', req.body)
    const { accountName, bankName, accountNumber} = req.body
    const id = req.params.id

    let createAccount = async () => {
        try {
            // Insert data into database
            let insertAccount = await database.into('Account').insert({
                user_id: id, 
                acc_name: accountName, 
                bank_name: bankName, 
                acc_number: accountNumber
            }) 
            res.status(200).end()
        } catch {
            error => res.send(error)
        }
    }
    createAccount()
};