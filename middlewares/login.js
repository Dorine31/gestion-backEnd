const bcrypt = require('bcrypt');
const knex = require('knex')
const jwt = require('jsonwebtoken');

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    let email = req.body.email
    // Check the password with the db password
    
    // Fetch id and password into database, if email exists
    let selectUser = await database.select('*').from('User').where({email: email})
    .then(selectUser => {
        let password = selectUser[0].password
        let id_user = selectUser[0].id
        // Compare passwords
        let passwordIsValid = bcrypt.compareSync(req.body.password, password)

        // Create a token 
        if (!passwordIsValid) {
            return res.status(401).send({auth: false, token: null})
        }
        let token = jwt.sign({ id:id_user }, 'secret')
        res.cookie("cookie", token, { maxAge: 10 * 60 * 1000 })
        res.status(200).send({auth: true, token: token, user: id_user})
        next()
    })
    .catch(err => res.status(401).send(err))
}