const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    if (req.path.startsWith('/register')) {
        // Fetch the data
        const { email, password } = req.body

        // Encrypt password
        const salRounds = 10
        const pswd = bcrypt.hashSync(password, salRounds)
 
        try {
            // Insert data into database
            let insertUser = await database.into('User').insert({
                email: email, password: pswd
            }) 
            res.end()
        } catch {
            error => res.send(error)
        }
    }
    next()
};

