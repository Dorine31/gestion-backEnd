const bcrypt = require('bcrypt');
const knex = require('knex')

// use knex and sqlite3
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res, next) => {
    // Fetch the data
    const {idAccount, movment, labelMvmt, amount, balance, dateMvmt} = req.body
    console.log(req.body.movment)
    const id = req.params.id
    let credit, debit
    if (movment === 'Créditer') {
        credit = parseFloat(amount)
        debit = 0
    } else if (movment === 'Débiter') {
        debit = parseFloat(amount)
        credit = 0
    }
    let newBalance = balance + credit - debit
    let createMvmt = async () => {
        try {
            // Insert data into database
            let create = await database.into('Movement').insert({acc_id: idAccount, mvmt_label: labelMvmt, credit: credit, debit: debit, mvmt_date: dateMvmt})
            res.status(200).send('ok')

        } catch (error) {
            res.send(error)
        }
    }

    //update account
    let updateAccount = async () => {
        console.log("update")

        try {
            console.log("balance", newBalance, "id", id, "account", idAccount, dateMvmt)
            // Insert data into database
            let update = await database.into('Account').update({balance: newBalance, balance_at: dateMvmt}).where({id: idAccount})
            
            res.end('Profil mis à jour.')
        } catch {
            error => res.send(error)
        }
    }
    createMvmt()
    updateAccount()
};
