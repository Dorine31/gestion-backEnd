
exports.up = function(knex) {
    return knex.schema
    .createTable('User', table => {
        table.increments('id').unsigned().primary()
        table.string('email').unique().notNullable()
        table.string('password').unique().notNullable()
        table.string('fullname')
        table.string('avatar_path')
        table.boolean('active').defaultTo(false)
        table.boolean('is_admin').defaultTo(false)
      }).createTable('Account', table => {
        table.increments('id').unsigned().primary()
        table.integer('user_id').references('id').inTable('User').notNull().onDelete('cascade')
        table.string('acc_name').notNullable()
        table.string('bank_name').notNullable()
        table.integer('acc_number').unique().notNullable()
        table.double('balance').notNullable().defaultTo(0.00)
        table.date('balance_at').notNullable().defaultTo('01/01/1900')
      }).createTable('Movement', table => {
        table.increments('id').unsigned().primary()
        table.integer('acc_id').references('id').inTable('Account').notNull().onDelete('cascade')
        table.string('mvmt_label').notNullable()
        table.double('credit')
        table.double('debit')
        table.date('mvmt_date').notNullable()
      })
};

exports.down = function(knex) {
  
};
